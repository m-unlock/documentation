# UNLOCK FAIR data platform

<br />

The UNLOCK **FAIR data platform**, is a vital part of the enabling character of the [UNLOCK facility](https://m-unlock.nl).

The heart of the UNLOCK FAIR data platform consists of two elements; WUR hosted **iRODS** data management, long-term storage and computing using **containerized applications and workflows**.

For data handling UNLOCK has adopted the **Resource Description Framework (RDF)** data-model as it enables the integration of independently created resources in a semantically structured framework.

<br><br><br>

![The unlock compute infrastructure](./figures/infra_cow.png)

