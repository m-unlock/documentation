# Workflows

Our workflows use Docker, reducing the need for complex dependencies while allowing us to incorporate many different tools. This approach relies on publicly avaliable images, so we use existing community images whenever possible and create and host our own when necessary.

**Installation and usage**

This section outlines and summarizes some of the existing workflows. For a full, detailed list of published workflows (including inputs, steps, and outputs), see our [WorkflowHub](https://workflowhub.eu/projects/16#workflows). An introduction to setting up and using one of these workflows can be found in the [](../setup/setup.md) section.

#### Workflow: Metagenomics Assembly

[View on Workflowhub](https://workflowhub.eu/workflows/367)

This workflow assembles genomes from Illumina reads and/or long reads. It is customizable to a certain extent regarding which steps to run and can also be used for isolates.

**Main steps involved:**
- [Illumina Quality Workflow](#workflow-illumina-quality)  
- [Long Read Quality Workflow](#workflow-long-reads-quality) 
- Assembly: SPAdes / Flye
- Short read polishing (Pilon)
- ONT read polishing (Medaka)
- QUAST (Assembly quality report)
- [Metagenomics Binning workflow](#workflow-metagenomics-binning)
- [Metagenomics GEM workflow](#workflow-metagenomics-gem)

---

#### Workflow: Illumina Quality

[View on Workflowhub](https://workflowhub.eu/workflows/336)

This workflow ensures high-quality Illumina read data before further analysis.  

**Steps included:**  
- FastQC quality plots (before and after filtering)
- fastp quality filtering
- BBduk PhiX removal and rRNA filtering
- BBmap Reference/contamination filtering (mapped or unmapped)
- Kraken2 taxonomic read classification (before and after)

---

#### Workflow: Long Reads Quality

[View on Workflowhub](https://workflowhub.eu/workflows/337)

This workflow ensures high-quality Nanopore/long-read data before further analysis.  

**Steps included:**  
- NanoPlot quality plots and reports (before and after filtering)
- Filtlong long reads quality filtering
- Minimap2 Reference/contamination filtering (mapped or unmapped)
- Kraken2 taxonomic read classification (before and after)

---

#### Workflow: Metagenomics Binning

[View on Workflowhub](https://workflowhub.eu/workflows/64)

This workflow bins metagenomic reads into individual genomes.  

**Steps included:**  
- Metabat2 / MaxBin2 / SemiBin binning
- DAS Tool bin refinement
- EukRep (eukaryotic classification)
- CheckM bin quality
- BUSCO bin quality
- GTDB-Tk bin taxonomic classification

---

#### Workflow: Metagenomics GEM

[View on Workflowhub](https://workflowhub.eu/workflows/372)

***!! Important caveat:***
*The CarveMe, MEMOTA and SMETANA Docker container images used in this workflow include the licenced CPLEX Optimizer. Therefore, we cannot make these images public. This means the workflow will not work out-of-the-box. However, we have made the Docker Build files available [here](https://gitlab.com/m-unlock/docker/-/tree/main/cwl/builder/docker)*

**Steps included:**  
- Prodigal protein prediction
- CarveMe GEnome-scale Metabolic model reconstruction
- MEMOTE for metabolic model testing
- SMETANA Species METabolic interaction ANAlysis