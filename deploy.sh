#!/bin/bash
#=======================================================================================
#title          :Unlock docs deploy
#description    :Deploy Unlock Documentation
#author         :Bart Nijsse & Jasper Koehorst
#date           :2023
#version        :0.0.3
#=======================================================================================

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR

# Build book
jupyter-book clean . && jupyter-book build .
# Include separate HTML
cp docs/tutorials/amplicon_tutorial.html _build/html/docs/tutorials/